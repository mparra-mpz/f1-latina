from operator import le
import numpy as np
from itertools import combinations

class F1Object:
    def __init__(self, name, points, value):
        self.name = name.replace('_', ' ')
        self.points = float(points)
        self.value = float(value)


if __name__ == '__main__':
    teams_file = '../config/teams.txt'
    teams = np.loadtxt(teams_file, dtype=np.str, comments='#')

    teams_candidates = []
    for t in teams:
        aux = F1Object(t[0], t[1], t[2])
        teams_candidates.append(aux)

    drivers_file = '../config/drivers.txt'
    drivers = np.loadtxt(drivers_file, dtype=np.str, comments='#')

    drivers_candidates = []
    for e in drivers:
        aux = F1Object(e[0], e[1], e[2])
        drivers_candidates.append(aux)

    drv_teams = list(combinations(drivers_candidates, 5))

    scuderia_candidates = []
    for team in teams_candidates:
        for drv in drv_teams:
            aux = [team] + list(drv)
            scuderia_candidates.append(aux)

    max_value = 115.979
    higher_points = -9.0
    best_scuderia = None
    for scud in scuderia_candidates:
        value = 0
        points = 0
        for i in scud:
            value += i.value
            points += i.points
        if value <= max_value and points > higher_points:
            higher_points = points
            best_scuderia = scud
    
    value = 0.0
    points = 0.0
    for obj in best_scuderia:
        print(obj.name)
        points += obj.points
        value += obj.value
    print('Points: %.2lf -- Value: %.2lf' % (points, value))
